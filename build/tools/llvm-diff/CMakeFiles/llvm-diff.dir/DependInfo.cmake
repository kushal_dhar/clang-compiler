# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/tools/llvm-diff/DiffConsumer.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/llvm-diff/CMakeFiles/llvm-diff.dir/DiffConsumer.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/llvm-diff/DiffLog.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/llvm-diff/CMakeFiles/llvm-diff.dir/DiffLog.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/llvm-diff/DifferenceEngine.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/llvm-diff/CMakeFiles/llvm-diff.dir/DifferenceEngine.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/llvm-diff/llvm-diff.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/llvm-diff/CMakeFiles/llvm-diff.dir/llvm-diff.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/llvm-diff"
  "../tools/llvm-diff"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/gpfs/home/kdhar/llvm/llvm/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/IRReader/CMakeFiles/LLVMIRReader.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/AsmParser/CMakeFiles/LLVMAsmParser.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/Demangle/CMakeFiles/LLVMDemangle.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/unittests/Rewrite/RewriteBufferTest.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/unittests/Rewrite/CMakeFiles/RewriteTests.dir/RewriteBufferTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CLANG_ENABLE_ARCMT"
  "CLANG_ENABLE_OBJC_REWRITER"
  "CLANG_ENABLE_STATIC_ANALYZER"
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/unittests/Rewrite"
  "../tools/clang/unittests/Rewrite"
  "../tools/clang/include"
  "tools/clang/include"
  "include"
  "../include"
  "../utils/unittest/googletest/include"
  "../utils/unittest/googlemock/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/gpfs/home/kdhar/llvm/llvm/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/utils/unittest/UnitTestMain/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/utils/unittest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Rewrite/CMakeFiles/clangRewrite.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Lex/CMakeFiles/clangLex.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Basic/CMakeFiles/clangBasic.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/gpfs/home/kdhar/llvm/llvm/build/lib/Demangle/CMakeFiles/LLVMDemangle.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

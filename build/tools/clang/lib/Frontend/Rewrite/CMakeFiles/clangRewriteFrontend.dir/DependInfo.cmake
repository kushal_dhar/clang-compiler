# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/FixItRewriter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/FixItRewriter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/FrontendActions.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/FrontendActions.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/HTMLPrint.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/HTMLPrint.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/InclusionRewriter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/InclusionRewriter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/RewriteMacros.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/RewriteMacros.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/RewriteModernObjC.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/RewriteModernObjC.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/RewriteObjC.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/RewriteObjC.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Frontend/Rewrite/RewriteTest.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Frontend/Rewrite/CMakeFiles/clangRewriteFrontend.dir/RewriteTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CLANG_ENABLE_ARCMT"
  "CLANG_ENABLE_OBJC_REWRITER"
  "CLANG_ENABLE_STATIC_ANALYZER"
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/lib/Frontend/Rewrite"
  "../tools/clang/lib/Frontend/Rewrite"
  "../tools/clang/include"
  "tools/clang/include"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

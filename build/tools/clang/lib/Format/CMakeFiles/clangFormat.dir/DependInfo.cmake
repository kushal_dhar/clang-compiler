# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/AffectedRangeManager.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/AffectedRangeManager.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/BreakableToken.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/BreakableToken.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/Comments.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/Comments.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/ContinuationIndenter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/ContinuationIndenter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/Format.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/Format.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/FormatToken.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/FormatToken.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/FormatTokenLexer.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/FormatTokenLexer.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/SortJavaScriptImports.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/SortJavaScriptImports.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/TokenAnalyzer.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/TokenAnalyzer.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/TokenAnnotator.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/TokenAnnotator.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/UnwrappedLineFormatter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/UnwrappedLineFormatter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/UnwrappedLineParser.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/UnwrappedLineParser.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/tools/clang/lib/Format/WhitespaceManager.cpp" "/gpfs/home/kdhar/llvm/llvm/build/tools/clang/lib/Format/CMakeFiles/clangFormat.dir/WhitespaceManager.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CLANG_ENABLE_ARCMT"
  "CLANG_ENABLE_OBJC_REWRITER"
  "CLANG_ENABLE_STATIC_ANALYZER"
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/lib/Format"
  "../tools/clang/lib/Format"
  "../tools/clang/include"
  "tools/clang/include"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/thirdparty/ittnotify/ittnotify_static.c" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/thirdparty/ittnotify/ittnotify_static.c.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/z_Linux_asm.s" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/z_Linux_asm.s.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "projects/openmp/runtime/src"
  "../projects/openmp/runtime/src"
  "include"
  "../include"
  "../projects/openmp/runtime/src/i18n"
  "../projects/openmp/runtime/src/include/45"
  "../projects/openmp/runtime/src/thirdparty/ittnotify"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_affinity.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_affinity.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_alloc.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_alloc.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_atomic.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_atomic.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_barrier.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_barrier.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_cancel.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_cancel.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_csupport.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_csupport.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_debug.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_debug.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_dispatch.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_dispatch.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_environment.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_environment.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_error.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_error.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_ftn_cdecl.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_ftn_cdecl.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_ftn_extra.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_ftn_extra.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_global.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_global.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_gsupport.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_gsupport.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_i18n.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_i18n.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_io.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_io.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_itt.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_itt.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_lock.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_lock.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_runtime.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_runtime.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_sched.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_sched.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_settings.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_settings.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_str.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_str.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_taskdeps.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_taskdeps.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_tasking.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_tasking.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_taskq.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_taskq.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_threadprivate.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_threadprivate.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_utility.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_utility.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_version.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_version.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/kmp_wait_release.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/kmp_wait_release.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/projects/openmp/runtime/src/z_Linux_util.cpp" "/gpfs/home/kdhar/llvm/llvm/build/projects/openmp/runtime/src/CMakeFiles/omp.dir/z_Linux_util.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "projects/openmp/runtime/src"
  "../projects/openmp/runtime/src"
  "include"
  "../include"
  "../projects/openmp/runtime/src/i18n"
  "../projects/openmp/runtime/src/include/45"
  "../projects/openmp/runtime/src/thirdparty/ittnotify"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

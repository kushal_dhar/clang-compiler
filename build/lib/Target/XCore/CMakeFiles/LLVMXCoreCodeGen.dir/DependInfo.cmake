# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreAsmPrinter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreAsmPrinter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreFrameLowering.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreFrameLowering.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreFrameToArgsOffsetElim.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreFrameToArgsOffsetElim.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreISelDAGToDAG.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreISelDAGToDAG.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreISelLowering.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreISelLowering.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreInstrInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreInstrInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreLowerThreadLocal.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreLowerThreadLocal.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreMCInstLower.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreMCInstLower.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreMachineFunctionInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreMachineFunctionInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreRegisterInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreRegisterInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreSelectionDAGInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreSelectionDAGInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreSubtarget.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreSubtarget.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreTargetMachine.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreTargetMachine.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/XCore/XCoreTargetObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/XCore/CMakeFiles/LLVMXCoreCodeGen.dir/XCoreTargetObjectFile.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Target/XCore"
  "../lib/Target/XCore"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

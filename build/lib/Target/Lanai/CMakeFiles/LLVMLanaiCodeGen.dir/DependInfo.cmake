# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiAsmPrinter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiAsmPrinter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiDelaySlotFiller.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiDelaySlotFiller.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiFrameLowering.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiFrameLowering.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiISelDAGToDAG.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiISelDAGToDAG.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiISelLowering.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiISelLowering.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiInstrInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiInstrInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiMCInstLower.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiMCInstLower.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiMachineFunctionInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiMachineFunctionInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiMemAluCombiner.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiMemAluCombiner.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiRegisterInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiRegisterInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiSelectionDAGInfo.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiSelectionDAGInfo.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiSubtarget.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiSubtarget.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiTargetMachine.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiTargetMachine.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Target/Lanai/LanaiTargetObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Target/Lanai/CMakeFiles/LLVMLanaiCodeGen.dir/LanaiTargetObjectFile.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Target/Lanai"
  "../lib/Target/Lanai"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

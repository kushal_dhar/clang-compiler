# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/ObjectYAML/COFFYAML.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ObjectYAML/CMakeFiles/LLVMObjectYAML.dir/COFFYAML.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ObjectYAML/DWARFYAML.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ObjectYAML/CMakeFiles/LLVMObjectYAML.dir/DWARFYAML.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ObjectYAML/ELFYAML.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ObjectYAML/CMakeFiles/LLVMObjectYAML.dir/ELFYAML.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ObjectYAML/MachOYAML.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ObjectYAML/CMakeFiles/LLVMObjectYAML.dir/MachOYAML.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ObjectYAML/ObjectYAML.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ObjectYAML/CMakeFiles/LLVMObjectYAML.dir/ObjectYAML.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ObjectYAML/YAML.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ObjectYAML/CMakeFiles/LLVMObjectYAML.dir/YAML.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/ObjectYAML"
  "../lib/ObjectYAML"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

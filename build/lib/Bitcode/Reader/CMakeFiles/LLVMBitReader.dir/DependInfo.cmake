# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/Bitcode/Reader/BitReader.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/BitReader.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Bitcode/Reader/BitcodeReader.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/BitcodeReader.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Bitcode/Reader/BitstreamReader.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/BitstreamReader.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Bitcode/Reader/MetadataLoader.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/MetadataLoader.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Bitcode/Reader/ValueList.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/ValueList.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Bitcode/Reader"
  "../lib/Bitcode/Reader"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

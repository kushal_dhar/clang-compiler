# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/Archive.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/Archive.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/ArchiveWriter.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/ArchiveWriter.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/Binary.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/Binary.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/COFFObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/COFFObjectFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/Decompressor.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/Decompressor.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/ELF.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/ELF.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/ELFObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/ELFObjectFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/Error.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/Error.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/IRObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/IRObjectFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/MachOObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/MachOObjectFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/MachOUniversal.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/MachOUniversal.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/ModuleSummaryIndexObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/ModuleSummaryIndexObjectFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/ModuleSymbolTable.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/ModuleSymbolTable.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/Object.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/Object.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/ObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/ObjectFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/RecordStreamer.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/RecordStreamer.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/SymbolSize.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/SymbolSize.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/SymbolicFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/SymbolicFile.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Object/WasmObjectFile.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Object/CMakeFiles/LLVMObject.dir/WasmObjectFile.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Object"
  "../lib/Object"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

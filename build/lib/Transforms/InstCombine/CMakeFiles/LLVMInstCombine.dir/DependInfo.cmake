# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineAddSub.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineAddSub.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineAndOrXor.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineAndOrXor.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineCalls.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineCalls.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineCasts.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineCasts.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineCompares.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineCompares.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineLoadStoreAlloca.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineLoadStoreAlloca.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineMulDivRem.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineMulDivRem.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombinePHI.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombinePHI.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineSelect.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineSelect.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineShifts.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineShifts.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineSimplifyDemanded.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineSimplifyDemanded.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstCombineVectorOps.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineVectorOps.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/Transforms/InstCombine/InstructionCombining.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstructionCombining.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Transforms/InstCombine"
  "../lib/Transforms/InstCombine"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/Caching.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/Caching.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/LTO.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTO.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/LTOBackend.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTOBackend.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/LTOCodeGenerator.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTOCodeGenerator.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/LTOModule.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTOModule.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/ThinLTOCodeGenerator.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/ThinLTOCodeGenerator.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/LTO/UpdateCompilerUsed.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/LTO/CMakeFiles/LLVMLTO.dir/UpdateCompilerUsed.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/LTO"
  "../lib/LTO"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/ExecutionUtils.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/ExecutionUtils.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/IndirectionUtils.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/IndirectionUtils.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/NullResolver.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/NullResolver.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/OrcABISupport.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcABISupport.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/OrcCBindings.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcCBindings.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/OrcError.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcError.cpp.o"
  "/gpfs/home/kdhar/llvm/llvm/lib/ExecutionEngine/Orc/OrcMCJITReplacement.cpp" "/gpfs/home/kdhar/llvm/llvm/build/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcMCJITReplacement.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/ExecutionEngine/Orc"
  "../lib/ExecutionEngine/Orc"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
